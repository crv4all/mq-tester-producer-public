package com.crv.test.mq;

import java.io.Serializable;

public class CountingMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private final int count;

    public CountingMessage(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "CountingMessage [count=" + count + "]";
    }
}
