package com.crv.test.mq;

import java.io.Serializable;

public class CrvTextMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String text;

    public CrvTextMessage(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "CrvTextMessage [text=" + text + "]";
    }
}
