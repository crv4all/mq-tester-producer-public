package com.crv.test.mq.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.jms.JMSException;

@Startup
@Singleton
public class ApiProducer {
    private static final Logger LOG = LoggerFactory.getLogger(ApiProducer.class);
    private static final String MESSAGE = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";
    private static final int TIMES = 5000;

    @Inject
    private NewTransactionProducer newTransactionProducer;

    @PostConstruct
    public void produce() {
        LOG.info("Producing 5000 time the MESSAGE: {} ", MESSAGE);
        try {
            for (int i = 0; i < 5000; i++) {
                newTransactionProducer.produce(MESSAGE);
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
        LOG.info("Done producing messages");
    }
}