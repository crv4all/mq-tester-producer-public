package com.crv.test.mq.producer;

import com.crv.test.mq.CrvTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import java.util.UUID;

@Stateless
public class NewTransactionProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewTransactionProducer.class);
    @Resource(mappedName = "java:/jms/queue/foo")
    private Queue queue;

    @Inject
    private JMSContext context;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void produce(String message) throws JMSException {
        CrvTextMessage crvTextMessage = new CrvTextMessage(message + UUID.randomUUID());
        ObjectMessage objectMessage = context.createObjectMessage(crvTextMessage);
        //LOGGER.info("Posting message {} to queue", crvTextMessage);
        //LOGGER.info("JMSXGroupID: {}",substring);
        objectMessage.setStringProperty("JMSXGroupID", "anId'");

        context.createProducer().send(queue, objectMessage);
    }
}
